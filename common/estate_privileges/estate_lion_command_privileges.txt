#All scopes are country scope
#All parameters are optional

estate_lion_command_land_rights = {
	icon = privilege_grant_autonomy
	loyalty = 0.05
	influence = 0.05
	land_share = 5
	max_absolutism = -5
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
		governing_capacity = 100
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_lion_command_oratory_schools = {
	icon = privilege_diplo_power
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
		country_diplomatic_power = 1
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_lion_command_x = {
	#icon = 
	loyalty = 0.10
	influence = 0.10
	land_share = 0
	max_absolutism = -10
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_lion_command_war_room_funds = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
		mil_advisor_cost = -0.25
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_lion_command_exaltations_from_leadership = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	on_granted = {
		create_general = { tradition = 40 }
	}
	
	penalties = {
	}
	
	benefits = {
		free_leader_pool = 2
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_lion_command_management = {	# this need more details
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	on_granted = {
		if = {
			limit = {
				any_owned_province = {
					has_province_modifier = hob_lion_war_camp # make this check teh correct province/modifier
				}
			}
			custom_tooltip = "Add a cool modifier in the Lion War Camp"
		}
		else = {
			custom_tooltip = "Has no effects upon being granted."
		}
	}
	
	on_granted_province = {
		random_owned_province = {
			limit = { has_province_modifier = hob_lion_war_camp } # make this check teh correct province/modifier
			custom_tooltip = estate_lion_command_management_tt
			hidden_effect = { add_province_triggered_modifier = hob_lion_management }
		}
	}
	
	on_revoked = {
		custom_tooltip = "Remove the cool modifier in the Lion War Camp"
	}
	
	on_revoked_province = {
		random_owned_province = {
			limit = { has_province_modifier = hob_lion_management }
			custom_tooltip = revoke_estate_lion_command_management_tt
			hidden_effect = { remove_province_triggered_modifier = hob_lion_management }
		}
	}
	
	on_invalid = {
		custom_tooltip = "Remove the cool modifier in the Lion War Camp"
	}
	
	on_invalid_province = {
		random_owned_province = {
			limit = { has_province_modifier = hob_lion_management }
			custom_tooltip = revoke_estate_lion_command_management_tt
			hidden_effect = { remove_province_triggered_modifier = hob_lion_management }
		}
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	modifier_by_land_ownership = {
		global_unrest = -2
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_lion_command_ninyu_kikun_officers = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
		land_morale = 0.10
		general_cost = -0.10
	}
	
	conditional_modifier = {
		trigger = { faction_in_power = hob_lion_command }
		modifier = {
			army_tradition_decay = -0.05
		}
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}